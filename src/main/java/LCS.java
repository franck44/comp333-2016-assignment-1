
import java.util.Set;
import java.util.HashSet;

/**
 * Compute Longest Common Sequences
 */
public class LCS {

    /**
     * The strings for which the analyser is built
     */
    String s1;
    String s2;

    /**
     * Make an LCS analyser for two strings first and second
     *
     *  @param     first    The first string
     *  @param     second   The second string
     */
    public LCS(String first, String second) {
        s1 = first;
        s2 = second;
    }

    /**
     *  Length of a maximal common subsequence
     *  @return     The length of a maximal common subsequence for s1 and s2
     */
    public Integer computeMaxLengthCS()  {
        //  FIX IT WITH YOUR CODE
        return -1;
    }

    /**
     *  Number of common subsequences of maximal length
     *
     *  @return     The length of a maximal common subsequence for s1 and s2
     */
    public Integer computeNumberOfMaxLengthCS()  {
        //  FIX IT WITH YOUR CODE
        return -1;
    }

    /**
     *  Compute a witness maximal common subsequence
     *
     *  @return     A maximal common subsequence for s1 and s2
     */
    public String computeMaxCS()  {
        //  FIX IT WITH YOUR CODE
        return "\n";
    }

    /**
     *  Compute all the maximal common subsequences
     *
     *  @return     The set of maximal common subsequences for s1 and s2
     */
    public Set<String> computeAllMaxCS()  {
        //  FIX IT WITH YOUR CODE
        return new HashSet<String>();
    }

    /**
     *  Check if a string is an LCS of two other strings
     *
     *  @param     w   Third string
     *
     *  @return        True if and only if w is an LCS of s1 ans s2
     */
    public Boolean isAnLCS(String w)  {
        //  FIX IT WITH YOUR CODE
        return false;
    }

}
