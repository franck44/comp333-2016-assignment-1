
/**
 * Compute Shortest Common Superstrings
 */
public class SCS {

    /**
     * The strings for which the analyser is built
     */
    String s1;
    String s2;

    /**
     * Make an LCS analyser for two strings first and second
     *
     *  @param     first    The first string
     *  @param     second   The second string
     */
    public SCS(String first, String second) {
        s1 = first;
        s2 = second;
    }

    /**
     *  Number of shortest common superstrings
     *
     *  @return     The number of SCS for s1 and s2
     */
    public Integer computeNumberOf()  {
        //  FIX IT WITH YOUR CODE
        return -1;
    }

    /**
     *  Compute a witness SCS
     *
     *  @return     A shortest common superstring for s1 and s2
     */
    public String computeShortestSCS()  {
        //  FIX IT WITH YOUR CODE
        return "\n";
    }

    /**
     *  Check if a string is a SCS of two other strings
     *
     *  @param     w   Third string
     *
     *  @return        True if and only if w is a SCS of s1 ans s2
     */
    public Boolean isAnSCS(String w)  {
        //  FIX IT WITH YOUR CODE
        return false;
    }

}
