
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;

/**
 * Test suite for Longest Common Subsequences
 */
public class LCStests {

    /**
     * Test case triples. Two arguments and an answer.
     */
    private class TestCase<K,L,R> {

        /**
         * A test case specified as a triple
         *
         * @param     x   First operand of test case
         * @param     y   Second operand of test case
         * @param     z   Expected result of the test
         */
        public TestCase(K x, L y, R z) {
            val1 = x;
            val2 = y;
            val3 = z;
        }

        public K val1;
        public L val2;
        public R val3;
    }

    /**
     * Test for computing the length of a maximal commom subsequence
     */
    @Test
    public void testMaxLength() {

        /** Create a table of test cases */
        List<TestCase<String, String, Integer> > testSuite = new ArrayList<>();
        testSuite.add(new TestCase<String, String, Integer>("here", "there", 4));
        testSuite.add(new TestCase<String, String, Integer>("almost", "a", 1));

        /** Run all the tests */
        for (TestCase<String, String,Integer> t : testSuite) {

            //  get an LCS analyser for t.val1 and t.val2
            LCS l =  new LCS(t.val1, t.val2);

            //  assertEquals(msg, expected, actual)
            assertEquals(
                "computeMaxLengthCS(" + t.val1 + ", " + t.val2 +")",
                t.val3,
                l.computeMaxLengthCS()
                );
        }
    }

    /**
     * Test for computing number of maximal commom subsequence
     */
    @Test
    public void testNumberOfMaxCS() {

        /** Create a table of test cases */
        List<TestCase<String, String, Integer> > testSuite = new ArrayList<>();
        testSuite.add(new TestCase<String, String, Integer>("here", "there", 6));
        testSuite.add(new TestCase<String, String, Integer>("almost", "a", 12));

        /** Run all the tests */
        for (TestCase<String, String,Integer> t : testSuite) {

            //  get an LCS analyser for t.val1 and t.val2
            LCS l =  new LCS(t.val1, t.val2);

            //  assertEquals(msg, expected, actual)
            assertEquals(
                "computeNumberOfMaxLengthCS(" + t.val1 + "," + t.val2 + ")",
                t.val3,
                l.computeNumberOfMaxLengthCS()
                );
        }
    }
}
